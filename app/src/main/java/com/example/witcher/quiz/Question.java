package com.example.witcher.quiz;

import java.sql.SQLData;
import java.util.ArrayList;

public class Question {
    private String rightAnswer;
    private String question;
    private ArrayList<String> answers;

    public String getRightAnswer() {
        return rightAnswer;
    }
    public int getRightAnswerID() {
        return answers.indexOf(rightAnswer);
    }

    public String getQuestion() {
        return question;
    }

    public ArrayList<String> getAnswers() {
        return answers;
    }

    public Question(String rightAnswer, String question, ArrayList<String> answers){
        this.answers = answers;
        this.question = question;
        this.rightAnswer = rightAnswer;
    }
    public Question(){
        answers = new ArrayList<>();
    }

}
