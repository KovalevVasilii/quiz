package com.example.witcher.quiz;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class StartActivity extends AppCompatActivity {

    private int numOfAnswers;
    private int numOfRigthAnswers;
    private static int currQuestion;
    private ArrayList<Question> listOfQuestions;
    private QuestionManager questionManager;
    private Button button;
    private RadioGroup radioGroup;
    private RadioButton answer1;
    private RadioButton answer2;
    private RadioButton answer3;
    private RadioButton answer4;
    private TextView textView;
    private ArrayList<RadioButton> rbList;
    private String pathQuiz;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        button = findViewById(R.id.nextQuest);
        radioGroup = findViewById(R.id.radioGroup);
        answer1 = findViewById(R.id.radio_one);
        answer2 = findViewById(R.id.radio_two);
        answer3 = findViewById(R.id.radio_three);
        answer4 = findViewById(R.id.radio_four);
        textView = findViewById(R.id.textView);
        currQuestion = 0;
        rbList = new ArrayList<>();
        rbList.add(answer1);
        rbList.add(answer2);
        rbList.add(answer3);
        rbList.add(answer4);
        pathQuiz = getIntent().getExtras().getString("path","path");
        if(pathQuiz == "path"){
            AlertDialog.Builder builder = new AlertDialog.Builder(StartActivity.this);
            builder.setTitle("Wrong path to Quiz!");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });

            AlertDialog alert1 = builder.create();
            alert1.show();
        }


        ArrayList<Question> debugQuestion = new ArrayList<>();
        ArrayList<String> debugAnsw = new ArrayList<>();
        debugAnsw.add("Alina");
        debugAnsw.add("One");
        debugAnsw.add("Two");
        debugAnsw.add("Three");
        debugQuestion.add(new Question("Alina","Which women is the best?", debugAnsw));

        questionManager = new QuestionManager(debugQuestion);
        listOfQuestions = questionManager.getListOfQuestions();
        numOfAnswers = this.listOfQuestions.size();
        textView.setText(listOfQuestions.get(currQuestion).getQuestion());

        for (int i=0; i<rbList.size(); i++){
            rbList.get(i).setText(listOfQuestions.get(0).getAnswers().get(i));
        }
        if(numOfAnswers-1 == currQuestion){
            button.setText("Finish");
        }
            button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                 if(numOfAnswers-1 == currQuestion){
                     RadioButton radBut = findViewById(radioGroup.getCheckedRadioButtonId());
                     int checkedAnswer = radioGroup.indexOfChild(radBut);
                     int rightAnswer = listOfQuestions.get(currQuestion).getRightAnswerID();
                     if(checkedAnswer == rightAnswer){
                         numOfRigthAnswers++;
                     }
                     else{
                         AlertDialog.Builder builder = new AlertDialog.Builder(StartActivity.this);
                         builder.setTitle("Right answer: " + listOfQuestions.get(currQuestion).getRightAnswer()+
                         "\nYour score: " + numOfRigthAnswers + "/" + numOfAnswers);
                         builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                             @Override
                             public void onClick(DialogInterface dialog, int which) {
                                 Intent intent=new Intent();
                                 setResult(numOfRigthAnswers,intent);
                                 finish();
                             }

                         });
                         AlertDialog alert1 = builder.create();
                         alert1.show();
                     }
                     //currQuestion++;
                     AlertDialog.Builder builder = new AlertDialog.Builder(StartActivity.this);
                     builder.setTitle("Your score: " + numOfRigthAnswers + "/" + numOfAnswers);
                     builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                         @Override
                         public void onClick(DialogInterface dialog, int which) {
                             Intent intent=new Intent();
                             setResult(numOfRigthAnswers,intent);
                             finish();
                         }
                     });

                     AlertDialog alert1 = builder.create();
                     alert1.show();
                 }
                 else if(currQuestion < numOfAnswers){
                    RadioButton radBut = findViewById(radioGroup.getCheckedRadioButtonId());
                    int checkedAnswer = radioGroup.indexOfChild(radBut);
                    int rightAnswer = listOfQuestions.get(currQuestion).getRightAnswerID();
                    if(checkedAnswer == rightAnswer){
                        numOfRigthAnswers++;
                    }
                    else{
                        AlertDialog.Builder builder = new AlertDialog.Builder(StartActivity.this);
                        builder.setTitle("Right answer: " + listOfQuestions.get(currQuestion).getRightAnswer());
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent=new Intent();
                                setResult(numOfRigthAnswers,intent);
                                finish();
                            }

                        });
                        AlertDialog alert1 = builder.create();
                        alert1.show();
                    }
                    currQuestion++;
                }
                else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(StartActivity.this);
                    builder.setTitle("Your score: " + numOfRigthAnswers + "/" + numOfAnswers);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent=new Intent();
                            setResult(numOfRigthAnswers,intent);
                            finish();
                        }
                    });

                    AlertDialog alert1 = builder.create();
                    alert1.show();
                }
                if(currQuestion != numOfAnswers) {

                    textView.setText(listOfQuestions.get(currQuestion).getQuestion());

                    for (int i = 0; i < rbList.size(); i++) {
                        rbList.get(i).setText(listOfQuestions.get(currQuestion).getAnswers().get(i));
                    }
                }

            }
        });

    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Progress will be lose");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.setNeutralButton("Stay", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog alert1 = builder.create();
        alert1.show();

    }
}
