package com.example.witcher.quiz;

import java.sql.SQLData;
import java.util.ArrayList;

public class QuestionManager {
    private ArrayList<Question> listOfQuestions;
    private String data;

    public QuestionManager(){ this.listOfQuestions = new ArrayList<>();}

    public QuestionManager(ArrayList<Question> listOfQuestions){
        this.listOfQuestions = listOfQuestions;
    }

    public QuestionManager(String data){
        this.listOfQuestions = new ArrayList<>();
        this.data = data;
    }
    public void loadQuestionsFromDB(){}

    public void loadQuestionsFromFile(String data){
        this.data = data;

    }

    public ArrayList<Question> getListOfQuestions(){
        return this.listOfQuestions;
    }

}
