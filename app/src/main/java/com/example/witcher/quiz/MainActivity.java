package com.example.witcher.quiz;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {


    private Button button;
    private TextView txtView;
    private static int bestAnswers;
    private static int chechedQuiz;
    private String pathQuiz;
    public static final String APP_PREFERENCES = "my_settings";
    public static final String APP_BEST_SCORE = "best_score";
    public static final String APP_SOURCE_QUIZ_1 = "quiz_1";
    public static final String APP_SOURCE_QUIZ_2 = "quiz_2";
    public static final String APP_SOURCE_QUIZ_3 = "quiz_3";
    private SharedPreferences mSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mSettings = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);

        button = findViewById(R.id.button);
        if(mSettings.contains(APP_BEST_SCORE)){
            bestAnswers = mSettings.getInt(APP_BEST_SCORE,0);
        }
        txtView = findViewById(R.id.textView2);
        txtView.setText("Best Score: "+bestAnswers);
        chechedQuiz = 0;
        pathQuiz = APP_SOURCE_QUIZ_1;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, StartActivity.class);
                intent.putExtra("path",pathQuiz);
                startActivityForResult(intent,chechedQuiz);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode > bestAnswers){
            bestAnswers = resultCode;
            SharedPreferences.Editor editor = mSettings.edit();
            editor.putInt(APP_BEST_SCORE, bestAnswers);
            editor.apply();
            txtView.setText("Best Score: "+bestAnswers);

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putInt(APP_BEST_SCORE, bestAnswers);
        editor.apply();
    }

    @Override
    protected void onStop() {
        super.onStop();
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putInt(APP_BEST_SCORE, bestAnswers);
        editor.apply();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mSettings.contains(APP_BEST_SCORE)){
            bestAnswers = mSettings.getInt(APP_BEST_SCORE,0);
        }
        txtView.setText("Best Score: "+bestAnswers);
    }
}